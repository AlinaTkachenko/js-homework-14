const body = document.querySelector('body');
const img = document.querySelector('img');
const btnStop = document.querySelector('.stop');
const btnStart = document.querySelector('.start');
const btnStyle = document.querySelector('.btn-style');
const btn = document.querySelectorAll('button');
let srcImg = ['./img/1.jpg', './img/2.jpg', './img/3.JPG', './img/4.png'];
let count = 1;
let timerId = setInterval(showPicture, 3000);

function showPicture() {
    img.removeAttribute('src');
    img.setAttribute('src', `${srcImg[count]}`);

    if (count < 3) {
        count++;
    } else {
        count = 0;
    }
};

btnStop.addEventListener('click', function () {
    clearInterval(timerId);
});

btnStart.addEventListener('click', function () {
    count = srcImg.indexOf(img.getAttribute('src'));
    timerId = setInterval(showPicture, 3000);
});

btnStyle.addEventListener('click', function () {
    body.classList.toggle('style');
    btn.forEach(function (element) {
        element.classList.toggle('button-style');
    });

    sessionStorage.setItem('styleBody', body.classList.contains('style'));
    
});

window.onload = function () {  
    if(sessionStorage.getItem('styleBody') === 'true'){
        body.classList.add('style');
        btn.forEach(function (element) {
            element.classList.add('button-style');
        });
    };
};
    
    
